class User < ActiveRecord::Base

	validates :name, :gender, :address, presence: true, on: [:create, :update]
	validates :username, presence: true, uniqueness: true, length: { minimum: 5 }, on: [:create, :update]
	validates :email, presence: true, uniqueness: true, format: {with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i}, on: [:create, :update]
	validates :password, presence: true, length: { minimum: 6 }, on: [:create, :update]


	has_many :orders
end
