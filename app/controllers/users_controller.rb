class UsersController < ApplicationController
	def index
		@users = User.all
		@user = User.find(session[:user])
      	
      	respond_to do |format|
        	format.html
        	format.json {render json: @users if @user.role == 0}
      	end
	end

	def new
	end

	def create
		@user = User.new(user_params)
		@user.role = 2
		@user.status = true

		if @user.save

			@url = request.protocol.to_s + request.host_with_port.to_s  + "/verifica/" + @user.id.to_s
			#UserMailer.email_verification(@user.name, @user.email, @url).deliver
			UserMailer.email_verification(@user.name, @user.email, @url).deliver_now

			#redirect_to @user
			session[:user] = @user.id
			redirect_to @user
		else
			#render "new"
			render "/index"
		end
	end



	def edit
		@user = User.find(params[:id])
	end

	def update
		@user = User.find(params[:id])

		if params[:user][:email] != @user.email
			@url = request.protocol.to_s + request.host_with_port.to_s  + "/verifica/" + @user.id.to_s
			#UserMailer.email_verification(@user.name, @user.email, @url).deliver
			UserMailer.email_verification(@user.name, @user.email, @url).deliver_now
			flash[:notice] = 'Changes Done successfully, Email confirmation has been sent.'
			params[:user][:verified] = false
		end	


		if @user.update(user_params)
			redirect_to @user
			flash[:notice] = 'Changes Done successfully.'
		else
			render "edit"		
		end
	end

	def show
		@actual_date = Time.now
		@user = User.find(params[:id])
		
		if @user.role == 2
			@orders = Order.where(user_id: params[:id]).where.not(status: "deleted")
			@orders_history = Order.where(user_id: params[:id])
			#@orders = Order.where(user_id: params[:id])
		else
			@orders = Order.all.where(archivado: false).where.not(status: "deleted")
			@orders_history = Order.all.where(archivado: false)

		end

		#render "index"
	end

	def login
		user = User.where(username: user_params[:username]).first
		
		if user
			if user.status
				if user.password == user_params[:password]
					session[:user] = user.id
					redirect_to user
				else
					flash[:notice] = 'Invalid User or Password'
					render "/index"
				end
			else
				if user.password == user_params[:password]
					user.status = true
					if user.save
						flash[:notice] = '¡Welcome again, ' + user.name.capitalize
						session[:user] = user.id
						redirect_to user
					end
				else
					flash[:notice] = 'Invalid User or Password'
					render "/index"
				end
			end
		else
			flash[:notice] = 'Invalid User or Password'
			render "/index"
		end

	end

	def logout
		flash[:notice] = nil
		session[:user] = nil
		redirect_to root_path
	end

	def inactive
		user = User.find(params[:id])
		user.status = false

		if user.update(user_params)
			#flash[:notice] = 'Su cuenta ha sido desactivada'
			session[:user] = nil
			redirect_to root_path
		end
	end

	def index_archivados
		#@user = User.find(params[:id])
		@user = User.find(session[:user])
		@orders = Order.all.where(archivado: true)
	end

	def email_verification

		user = User.find(params[:id])
		#user = User.where( uuid: params[:uuid] ).first

		user.verified = true

		if user.save
		    flash[:notice] = "¡Your account has been activated!"
		    redirect_to root_path
		  else
		    flash[:notice] = "Your account cannot be activated"
		    render root_path
		end
		
	end

  def forgot_check

    time1 = Time.new
    user = User.where(email: user_params[:email]).first
    #user.forgot = time1.sec.to_s + time1.min.to_s + time1.hour.to_s + time1.day.to_s + time1.month.to_s + time1.year.to_s

    if user
      #if !user.provider_uid
        user.forgot = time1.hour.to_s + time1.day.to_s + time1.month.to_s + time1.year.to_s
        if user.save
          #url = request.protocol + request.host_with_port + "/forgot/" + user.uuid
          url = request.protocol + request.host_with_port + "/forgot/" + user.id.to_s
          #UserMailer.email_forgot(user.name, user.email, url).deliver
          UserMailer.email_forgot(user.name, user.email, url).deliver_now
          flash[:notice] = "Se te ha enviado un correo para cambiar de pass"
          redirect_to root_path
        end
      #else
        #flash[:notice] = "El correo está registrado con Facebook o Twitter"
        #render "/index"
      #end
    else
      flash[:notice] = "This Email doesn't exist"
      render "/index"
    end

  end

  def forgot_change
    time1 = Time.new
    #@user = User.where(uuid: params[:uuid]).first
    #puts "PARAMETRO="+params[:uuid]
    @user = User.find(params[:id])
    
    #actual_forgot = time1.sec.to_s + time1.min.to_s + time1.hour.to_s + time1.day.to_s + time1.month.to_s + time1.year.to_s
    actual_forgot = time1.hour.to_s + time1.day.to_s + time1.month.to_s + time1.year.to_s

    if @user.forgot == actual_forgot
      render "forgot_change"
    else
      flash[:notice] = "Errorseins, se vencio pide otra vez"
      redirect_to root_path
    end
  end 

   def forgot_success
    
    #@user = User.where(uuid: params[:uuid]).first
    @user = User.find(params[:id])
    
    puts "TAM:"+ params[:user][:password].length.to_s

    if params[:user][:password] != "" || params[:user][:password].length > 5

     #@user.password = Digest::SHA1.hexdigest(params[:user][:password])
     #params[:user][:password] = @user.password
     @user.forgot = nil

        if @user.update(user_params)
          flash[:notice] = "Ha sido cambiada tu pass"
          redirect_to root_path
        else
          flash[:notice] = "Errorseins"
          render "forgot_change"
        end
    else
      flash[:notice] = "Escribe una contraseña valida"
      render "forgot_change"
    end

  end

  def reconfirm
  	user = User.find(params[:id])
  	url = request.protocol.to_s + request.host_with_port.to_s  + "/verifica/" + user.id.to_s
	#UserMailer.email_verification(user.name, user.email, url).deliver
	UserMailer.email_verification(user.name, user.email, url).deliver_now
	redirect_to edit_user_path(user)
  end

	private
	def user_params
    	params.require(:user).permit(:uuid ,:name, :username, :gender, :address, :phone, :cellphone, :email, :password, :status, :verified, :role)	
	end
end