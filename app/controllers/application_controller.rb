class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def index
  	if session[:user]  		
  		@user = User.find(session[:user])
  		redirect_to user_path(@user)
  	else
  		render "/index"
  	end
  end

end
