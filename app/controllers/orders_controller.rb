class OrdersController < ApplicationController

	#skip_before_action :verify_authenticity_token

	def index
		@orders = Order.all
		@user = User.find(session[:user])
      	
      	respond_to do |format|
        	format.html
        	format.json {render json: @orders if @user.role == 0}
      	end
	end

	def new
		@user = User.find(params[:user_id])
	end

	def create
		

		@user = User.find(params[:user_id])
		@order = Order.new(order_params)
		@order.archivado = false

		if params[:order][:cant] && params[:order][:size]
			if params[:order][:cant].split(",").length == params[:order][:size].split(",").length

				@order.bill_status = false
				@order.cant = params[:order][:cant].split(",")
				@order.size = params[:order][:size].split(",")

				@order.price = 0

				for i in 0...@order.cant.length
					@order.price += Price.where(name: @order.cant[i]).where(size: @order.size[i]).first.value
				end

				@order.user_id = params[:user_id]
				@order.status = 'pending'
				@order.order_date = Time.now
				@order.expires_date = Time.now + 1.day
				@order.entrega_date = Time.now + 1.hour

				if @order.save
					redirect_to user_path(params[:user_id])
				else
					render 'new'
				end
			else
				flash[:notice] = 'NO HIJO'
				render 'new'
			end
		else
			render 'new'
		end

	end

	def calcula
		puts "valor: "+	params[:valor].to_s
	end

	def show
		@order = Order.find(params[:id])

		#@user = User.find(session[:user])
		#render "index"
	end

	def edit
		@user = User.find(params[:user_id])
		@order = Order.find(params[:id])
	end

	def update
		@order = Order.find(params[:id])


		if params[:order][:cant] && params[:order][:size]
			
			@order.cant = params[:order][:cant].split(",")
			@order.size = params[:order][:size].split(",")
			params[:order][:cant] = params[:order][:cant].split(",")
			params[:order][:size] = params[:order][:cant].split(",")

			if @order.update(order_params)
				redirect_to user_order_path(params[:user_id],@order)
			else
				render "edit"
			end
		else
			render "edit"
		end		
	end

	def destroy
		order = Order.find(params[:id])
		order.destroy
		redirect_to user_path(params[:user_id])
	end


	def change_status
		@user = User.find(session[:user])
		@order = Order.find(params[:id])

		if @user.role == 2
			@order.status = 'cancelled'
		else
			@order.status = order_params[:status]
		end

	    if @order.save
	      redirect_to user_path(@user)
	    end
	end

	def delete
		@user = User.find(session[:user])
		@order = Order.find(params[:id])

		if @user.role == 2
			@order.status = 'deleted'
		else
			@order.status = order_params[:status]
		end

	    if @order.save
	      redirect_to user_path(@user)
	    end
	end	

	def archivado
		@user = User.find(session[:user])
		@order = Order.find(params[:id])
		@order.archivado = true

		puts "USER_ORDER: " + params[:user_id] + params[:id]

	    if @order.save
	      redirect_to user_path(@user)
	    end
	end


	def new_payment
		@order=Order.find(params[:order_id])
	end

	def payment

		user=User.find(params[:user_id])
		@order=Order.find(params[:id])

		@amount = @order.price * 100
		Stripe.api_key = 'sk_test_tHKvI69La7NqUvVyhzKTvlRo'
		token = params[:stripeToken]
		# Create the charge on Stripe's servers - this will charge the user's card

		begin

		  charge = Stripe::Charge.create(
		    :amount => @amount, # amount in cents, again
		    :currency => "usd",
		    :source => token,
		    :description => "Example charge"
		  )

			@order.bill_status = true
			if @order.save
				redirect_to user_path(params[:user_id])
			else
				render user_path(params[:user_id])
			end

		rescue Stripe::CardError => e
		  # The card has been declined
			@order.bill_status = false
			if @order.save
				redirect_to user_path(params[:user_id])
			else
				render user_path(params[:user_id])
			end
		end

	end	
	

	private
	def order_params
		params.require(:order).permit(:user_id ,:cant, :size, :order_date, :expires_date, :entrega_date, :address, :status, :tipo, :archivado, :token, :price, :bill_status)	
	end
end
