class MsgsController < ApplicationController

	def create
		@msg = Msg.new(msg_params)
		@msg.user_id = params[:user_id]
		user = User.find(params[:user_id])
		#@user.role = 2
		#@user.status = true

		if @msg.save

			#@url = request.protocol.to_s + request.host_with_port.to_s  + "/verifica/" + @user.id.to_s
			#UserMailer.email_verification(@user.name, @user.email, @url).deliver

			UserMailer.email_contact(user.username, params[:msg][:email], params[:msg][:message], 'diegojz150@gmail.com').deliver

			#redirect_to @user
			#session[:user] = @user.id
			flash[:notice] = 'Your message has been sent.'
			redirect_to root_path
		else
			#render "new"
			render "/index"
		end
	end

	private
	def msg_params
    	params.require(:msg).permit(:user_id,:email,:subject,:message)	
	end	
end
