// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require_tree .

var str = new Array(10);
var vals_prod = [];
var vals_tam = [];

var vals_price = [];

var saveButton,lockSaveBtn,lockAdd = false;

var total = 0;
var suiche = true;

//var precios ={ "1", "2", "3"};
//var preciostwo ={ "4", "5", "6"};

var OpLechona = {
 "Small":"2",
 "Medium":"4",
 "Large":"5",
};

var OpEmpanada = {
 "Tree":"3",
 "Six":"5",
 "Nine":"8",
 "Twelve":"10",
};

selec_count = 0;
var value = ['',''];

idleTimer = null;
idleState = false;
idleWait = 60000;

$(document).ready(function(){ 

	//MENU SUPERIOR RESPONSIVO
	$( '.desk_options' ).hide();
	$( '.mob_options' ).show();


		if($( window ).width()<650){
			$( '.desk_options' ).hide();
			$( '.mob_options' ).show();
		}
		else{
			$( '.desk_options' ).show();
			$( '.mob_options' ).hide();
		}

	$( window ).resize(function() {
  		//console.log($( window ).width());

		if($( window ).width()<650){
			$( '.desk_options' ).hide();
			$( '.mob_options' ).show();
		}
		else{
			$( '.desk_options' ).show();
			$( '.mob_options' ).hide();
		}

	});


	$('*').bind('mousemove keydown scroll', function () {
        
            clearTimeout(idleTimer);
                    
            if (idleState == true) { 
                
                // Reactivated event
                //$(".user_page").append("<p>Welcome Back.</p>");
                location.reload();           
            }
            
            idleState = false;
            
            idleTimer = setTimeout(function () {
                
                // Idle Event
                //$(".user_page").append("<p>You've been idle for " + idleWait/1000 + " seconds.</p>");

                idleState = true; }, idleWait);
        });
        
        $("body").trigger("mousemove");



    // Codigo a ejecutar cuando el html esté listo.
    $('.modal_director').on('click', function(event) {
        event.preventDefault();
        var actor = $(this).attr('href');

        suiche=!suiche;

        if(suiche){
            $(actor).show(function() {
                //$(actor + ' .modal_content').slideDown();
                //$(actor + ' .modal_content').fadeIn();
                $('.modal_actor').animate({width:'toggle'},350);
            });
        }
        else{
            $('.modal_actor').animate({width:'toggle'},350);
            //$('.modal_actor').fadeOut();
        }
    });

    //console.log(suiche);

    $('.modal_close').on('click', function() {
       // $('.modal_actor').hide();
       $('.modal_actor').fadeOut();
    });
    $('.modal_actor').on('click', function() {
        //$(this).hide();
        $(this).fadeOut();
    });
    //MODAL DEL LADO


	$( '#rem' ).addClass( "disabled" );
	var res = window.location.pathname.split("/");
	//console.log(res);

	switch(res.length) {
    case 2:
        $('.ruta').html("LuzMA");
        break;
    case 3:
    	if(res[2] == ""){
        	$('.ruta').html("Usuarios");
        }
        else{
        	$('.ruta').html("Inicio");
        }
        break;
    case 4:
        $('.ruta').html("Profile");
        break;
    case 5:
        $('.ruta').html("Order");
        break;     
    case 6:
        $('.ruta').html("Modify");
        break;            
	}

	

	$('.modal_log').on('click', function() {
	  $('#login_modal').modal('show');
	});

	$('.modal_sig').on('click', function() {
	  $('#sign_modal').modal('show');
	});

	$('.modal_forg').on('click', function() {
	  $('#forgot_modal').modal('show');
	});

	$('.modal_hist').on('click', function() {
	  $('#history_modal').modal('show');
	});

	$('.modal_cont').on('click', function() {
	  $('#contact_modal').modal('show');
	});	


	/*
	for(var i=1;i<99;i++){

		value = '.modal_det_'+i

		$('.modal_det_'+i).on('click', function() {
		  $('#det_modal_'+i).modal('show');
		});
	}
	*/

	//$('.modal_det').on('click', function() {
	 // $('#det_modal').modal('show');
	//  showModal("Hola");
	//});


	$('.modal .close_modal').on('click', function() {
  		$(this).closest('.modal').modal('hide');
  		$(this).modal('handleUpdate');
	});

	//console.log(window.location.pathname);

	if(window.location.pathname == '/login'){
		$('#login_modal').modal('show');
	}

	if(window.location.pathname == '/users'){
		$('#sign_modal').modal('show');
	}

	if(window.location.pathname == '/forgot'){
		$('#forgot_modal').modal('show');
	}
	

	$( "#add" ).click(function() {

		if (selec_count<10){
			addSede("#field_div");
			selec_count++;
			console.log(selec_count);
			
			vals_prod = [];
			vals_tam = [];
			vals_price =[];
			total=0;
			var i=0;

			$( '.value_prod' ).each(function( index ) {
	  			vals_prod.push($( this ).val());
			});

		    $( '.value_tam option:selected' ).each(function() {
  				vals_tam.push($( this ).text());
			});

			$( '.value_tam' ).each(function( index ) {
	  			vals_price.push($( this ).val());
			});

			//console.log(vals_prod);
			//console.log(vals_tam);
			
			while (i < vals_price.length) {
			    total += parseInt(vals_price[i]);
			    i++;
			}
			$("#label_price").text("Total: $" + total.toString());

			$("#prods_bd_field").attr("value", vals_prod);
			$("#tams_bd_field").attr("value", vals_tam);

			if (selec_count == 1){
				$( '#rem' ).removeClass( "disabled" );
			}
			if (selec_count == 10){
				$( this ).addClass( "disabled" );
			}

		}

			
	});

	$( "#rem" ).click(function() {
		if (selec_count>0){
			
			$( '.campo_producto' ).last().remove();

			selec_count--;
			str[selec_count-1] = ""
			console.log(selec_count);

			vals_prod = [];
			vals_tam = [];
			vals_price =[];
			total=0;
			var i=0;

			$( '.value_prod' ).each(function( index ) {
	  			vals_prod.push($( this ).val());
			});

		    $( '.value_tam option:selected' ).each(function() {
  				vals_tam.push($( this ).text());
			});

			$( '.value_tam' ).each(function( index ) {
	  			vals_price.push($( this ).val());
			});

			//console.log(vals_prod);
			//console.log(vals_tam);
			
			while (i < vals_price.length) {
			    total += parseInt(vals_price[i]);
			    i++;
			}
			$("#label_price").text("Total: $" + total.toString());

			$("#prods_bd_field").attr("value", vals_prod);
			$("#tams_bd_field").attr("value", vals_tam);
			if (selec_count == 0){
				$( this ).addClass( "disabled" );
			}
			if (selec_count == 9){
				$( '#add' ).removeClass( "disabled" );
			}
		}
	});


	setInterval(function(){
		

		$( ".edit" ).click(function() {
			total = 0;
			numSedes=0;
			vals_prod = [];
			vals_tam = [];
			vals_price = [];
			final_sedes = [];
			$(".value_prod").attr("disabled",false);
			$(".value_tam").attr("disabled",false);

			$("#prods_bd_field").attr("value", "");
			$("#tams_bd_field").attr("value", "");

			$( '.edit' ).remove();
			$('.add_rem').show();

			lockSaveBtn=false;
			lockAdd=true;
		});
	}, 1000);



});


function addSede(id){
	$( id ).append(
				"<div class='campo_producto' style='display: none;'>" +

					"<div class='form-group'>"+

			  			"<div class='input-group'>"+
					    	"<span class='input-group-addon'>Product</span>"+

							"<select class= 'form-control value_prod' id= 'prod" + selec_count + "' >" + 

							  "<option value='lechona'>Lechona</option>" +
							  "<option value='empanada'>Empanada</option>" +
							  "<option value='tamal'>Tamal</option>" +
							"</select>" +
						"</div>" +

						"<div class='input-group'  style='margin-top:5px;'>"+
							"<span class='input-group-addon'>Size</span>"+
								"<select class= 'form-control value_tam' id= 'tam" + selec_count + "' >" + 
								  "<option value='2'>Small</option>" +
								  "<option value='4'>Medium</option>" +
								  "<option value='5'>Big</option>" +
								"</select> <br>" +
					  	"</div>" +

					"</div>" +

				"</div>"	
					 );

		$('.campo_producto').show('fast');
		var producto =  "#prod" + selec_count;
		var tamano =  "#tam" + selec_count;

		//INICIALIZACION
	    $( producto + " option:selected" ).each(function() {
  			str[selec_count] = $( this ).text();
		});	

		//console.log( str );

		$( producto ).change(function() {
		    //var str = new Array(10);
		    //var str = "";
		    
		    $( producto + " option:selected" ).each(function() {
      			str[selec_count-1] = $( this ).text();
    		});		

		    //console.log( str );
		    //alert( str );

		    if (str[selec_count-1] == "Lechona"){
		    	//alert( str );
		    	$( tamano ).empty(); 
		    	$.each(OpLechona, function(key, value) {
				 $( tamano ).append($("<option></option>")
				 .attr("value", value).text(key));
				});		    	
		    }
		    else if(str[selec_count-1] == 'Empanada'){
		    	$( tamano ).empty();
		    	$.each(OpEmpanada, function(key, value) {
				 $( tamano ).append($("<option></option>")
				 .attr("value", value).text(key));
				});

		    }
		    else{
		    	$( tamano ).empty();
		    	$.each(OpLechona, function(key, value) {
				 $( tamano ).append($("<option></option>")
				 .attr("value", value).text(key));
				}); 
		    }



		});

	if(!saveButton){
		$( '#saveB' ).append("<a class='save'>Guardar</a><br>");
		saveButton=true;
	}
	
	$( '.value_prod' ).change(function () {
			vals_prod = [];
			vals_tam = [];
			vals_price =[];
			total=0;
			var i=0;

			$( '.value_prod' ).each(function( index ) {
	  			vals_prod.push($( this ).val());
			});

		    $( '.value_tam option:selected' ).each(function() {
  				vals_tam.push($( this ).text());
			});

			$( '.value_tam' ).each(function( index ) {
	  			vals_price.push($( this ).val());
			});

			//console.log(vals_prod);
			//console.log(vals_tam);
			
			while (i < vals_price.length) {
			    total += parseInt(vals_price[i]);
			    i++;
			}
			$("#label_price").text("Total: $" + total.toString());

			$("#prods_bd_field").attr("value", vals_prod);
			$("#tams_bd_field").attr("value", vals_tam);
	 });

	$( '.value_tam' ).change(function () {
			vals_prod = [];
			vals_tam = [];
			vals_price =[];
			total=0;
			var i=0;

			$( '.value_prod' ).each(function( index ) {
	  			vals_prod.push($( this ).val());
			});

		    $( '.value_tam option:selected' ).each(function() {
  				vals_tam.push($( this ).text());
			});

			$( '.value_tam' ).each(function( index ) {
	  			vals_price.push($( this ).val());
			});

			//console.log(vals_prod);
			//console.log(vals_tam);
			
			while (i < vals_price.length) {
			    total += parseInt(vals_price[i]);
			    i++;
			}
			$("#label_price").text("Total: $" + total.toString());

			$("#prods_bd_field").attr("value", vals_prod);
			$("#tams_bd_field").attr("value", vals_tam);
	 });

	$( ".save" ).click(function() {



		if (!lockSaveBtn){

			var i = 0;

			$( '.value_prod' ).each(function( index ) {
	  			vals_prod.push($( this ).val());
			});

		    $( '.value_tam option:selected' ).each(function() {
  				vals_tam.push($( this ).text());
			});

			$( '.value_tam' ).each(function( index ) {
	  			vals_price.push($( this ).val());
			});

			console.log(vals_prod);
			console.log(vals_tam);

			//$( "#sedes_bd_field" ).append( final_sedes );

			$("#prods_bd_field").attr("value", vals_prod);
			$("#tams_bd_field").attr("value", vals_tam);


			//SUMATORIA DEL TOTAL
			while (i < vals_price.length) {
			    total += parseInt(vals_price[i]);
			    i++;
			}
			console.log("TOTAL= " + total);

			//var recursiveEncoded = $.param( vals_tam );

			//var serializedArr = JSON.stringify( vals_tam );
			//var unpackArr = JSON.parse( serializedArr );
			
			$("#label_price").text("$" + total.toString());
			$("h2").text("$" + total.toString());


			$(".value_prod").attr("disabled",true);
			$(".value_tam").attr("disabled",true);
			$( '#editB' ).append("<a class='edit'>Editar</a>");
			
			$('.add_rem').hide();

			lockAdd=false;
		}

		lockSaveBtn=true;

	});
}


function tipo(o) {
	return TYPES[typeof o] || TYPES[TOSTRING.call(o)] || (o ? 'object' : 'null');
};

function showModal(data)
{
   //you can do anything with data, or pass more data to this function. i set this data to modal header for example
   $("#det_modal .modal-title").html('Order #'+data[0])
   $("#det_modal #adr").html(data[1])
   $("#det_modal #tip").html(data[2])
   $("#det_modal #ent").html(data[3])
   $("#det_modal").modal();
}