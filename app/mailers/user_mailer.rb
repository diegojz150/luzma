class UserMailer < ApplicationMailer

	default from: 'info@luzma.com'

	def email_verification(username, email, url)
		@name = username
		@url = url
		@email = email
		mail(to: @email, subject: "Bienvenido")
	end

	def email_forgot(username, email, url)
		@name = username
		@url = url
		@email = email
		mail(to: @email, subject: "Recuperación")
	end

	def email_contact(username, email, message, to_email)
		@name = username
		@message = message
		@email = email
		@to_email = to_email
		mail(to: @to_email, subject: "Contacto")
	end
end
