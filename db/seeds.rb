# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create(name: 'admin', username: 'admin', gender: 'male', address: 'None', phone: '12345', cellphone: '12345678',email: 'admin@admin.com', password: 'america', status: true, verified: true, role: 0)
User.create(name: 'venta', username: 'venta', gender: 'male', address: 'None', phone: '12345', cellphone: '12345678',email: 'venta@admin.com', password: 'america', status: true, verified: true, role: 1)
User.create(name: 'diego', username: 'diego', gender: 'male', address: 'None', phone: '12345', cellphone: '12345678',email: 'diego@admin.com', password: 'america', status: true, verified: true, role: 2)

Price.create(name: 'lechona', size: 'Small', value: '2')
Price.create(name: 'lechona', size: 'Medium', value: '4')
Price.create(name: 'lechona', size: 'Big', value: '5')


Price.create(name: 'empanada', size: 'Tree', value: '3')
Price.create(name: 'empanada', size: 'Six', value: '5')
Price.create(name: 'empanada', size: 'Nine', value: '8')
Price.create(name: 'empanada', size: 'Twelve', value: '10')

Price.create(name: 'tamal', size: 'Small', value: '2')
Price.create(name: 'tamal', size: 'Medium', value: '4')
Price.create(name: 'tamal', size: 'Big', value: '5')