# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150917212722) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "msgs", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "email"
    t.string   "subject"
    t.string   "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "msgs", ["user_id"], name: "index_msgs_on_user_id", using: :btree

  create_table "orders", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "cant",         default: [],              array: true
    t.string   "size",         default: [],              array: true
    t.datetime "order_date"
    t.datetime "expires_date"
    t.datetime "entrega_date"
    t.string   "address"
    t.string   "status"
    t.string   "tipo"
    t.boolean  "archivado"
    t.integer  "price"
    t.boolean  "bill_status"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "orders", ["user_id"], name: "index_orders_on_user_id", using: :btree

  create_table "prices", force: :cascade do |t|
    t.string   "name"
    t.string   "size"
    t.integer  "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "uuid"
    t.string   "name"
    t.string   "username"
    t.string   "gender"
    t.string   "address"
    t.string   "phone"
    t.string   "cellphone"
    t.string   "email"
    t.string   "password"
    t.boolean  "verified"
    t.boolean  "status"
    t.integer  "role"
    t.string   "forgot"
    t.string   "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "msgs", "users"
  add_foreign_key "orders", "users"
end
