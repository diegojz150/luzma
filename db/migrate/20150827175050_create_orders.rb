class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.references :user, index: true, foreign_key: true
      t.string :cant, array: true, default: []
      t.string :size, array: true, default: []
      t.datetime :order_date
      t.datetime :expires_date
      t.datetime :entrega_date
      t.string :address
      t.string :status
      t.string :tipo
      t.boolean :archivado
      t.integer :price
      t.boolean :bill_status
      
      t.timestamps null: false
    end
  end
end
