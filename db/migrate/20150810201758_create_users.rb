class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :uuid
      t.string :name
      t.string :username
      t.string :gender
      t.string :address
      #t.integer :phone
      #t.integer :cellphone
      t.string :phone
      t.string :cellphone
      t.string :email
      t.string :password
      t.boolean :verified
      t.boolean :status
      t.integer :role
      t.string :forgot
      t.string :image

      t.timestamps null: false
    end
  end
end
