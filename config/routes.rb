Rails.application.routes.draw do
  #get 'orders/index:users'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'
  root 'application#index'

  resources :users

  resources :users do
    post "/contact" => "users#contact", as: "contact"
    resources :orders
    resources :msgs
    patch "/orders/:id/archivado" => "orders#archivado", as: "archivado"
    patch "/orders/:id/change_status" => "orders#change_status", as: "change_status"
    patch "/orders/:id/delete" => "orders#delete", as: "delete_order"
    post "/orders/:id/payment" => "orders#payment", as: "payment"
    post "/orders/calcula" => "orders#calcula", as: "calcula"
  end
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  get "/logout" => "users#logout", as: "logout"
  post "/login" => "users#login", as: "login"
  patch "/users/:id/inactive" => "users#inactive", as: "inactive"
  
  #patch "/users/:id/orders/:id/change_status" => "orders#change_status", as: "change_status"
  #patch "/users/:user/orders/:id/archivado" => "orders#archivado", as: "archivado"

  post "/users/:id/reconfirm" => "users#reconfirm", as: "reconfirm"

  get "/users/:id/index_archivados" => 'users#index_archivados', as: "index_archivados"

  get "verifica/:id", to: "users#email_verification", as: "confirmemail"


  post "/forgot" => "users#forgot_check", as: "forgot_check"
  get "forgot/:id" => "users#forgot_change", as: "forgot_change"
  patch "/forgot/:id/success" => "users#forgot_success", as: "forgot_success"

  #get '/users/:id/index_archivados' => 'users#index_archivados', as: "index_archivados"
  #patch '/users/:id/orders/:order_id/payment' => "orders#payment", as: "payment"
  
  #get "forgot/:uuid" => "users#forgot_change", as: "forgot_change"
  #patch "/forgot/:uuid/success" => "users#forgot_success", as: "forgot_success"

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products


  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
